**Overview**

This API Is Based On Node.js

**Installation Guide**


**Before You Begin**

1.Make Sure That You Have Installed Node.js(15.14.0 Or Higher)
And Git


2.Clone Repository

3.Change Your Directory To bookAPI with `cd bookAPI` After Cloning The Repository

4.Run `npm install` to Install The Necessary Dependencies

5.After That Run `npm start` Or `npm run start` To Start Your Api Locally

6.Open Up Your Browser Then Type `localhost:3000` The Api Will Run On 3000 port You Can Change It From `app.js`

7.Then If You Want The List Of The Books type `localhost:3000/api/books` 

8.You Can Search Books With Its Genre,Author,Title,ID 



**Contribution Guide**


Please take a look at the contributing  guidelines if
you're interested in helping by any means.
Contribution to this project is not only limited to coding help, You can
suggest a feature, help with docs, design ideas or even some typos. You
are just an issue away.  Don't hesitate to create an issue.
❤️
