// eslint-disable-next-line no-unused-vars
const should = require('should');
const sinon = require('sinon');
const bookController = require('../controllers/booksController');

describe('BookControllerTests:', () => {
  describe('Post', () =>{
    it('Should not Allow An Empty Title On Post', () => {
      const Book = function(book) {
        this.save = () => {};
        const req = {
          body: {
            author: 'asd',
          },
        };
        const res = {
          status: sinon.spy(),
          send: sinon.spy(),
          json: sinon.spy(),
        };
        // eslint-disable-next-line no-unused-vars
        const controller = bookController(Book);
        controller.post(req, res);
        // eslint-disable-next-line max-len
        res.status.calledwith(400).should.equal(true, `Bad Status ${req.status.args[0][0]}`);
        res.send.calledWith('Title Is required').should.equal(true);
      };
    });
  });
});
