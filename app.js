const express = require('express');

const mongoose = require('mongoose');

const bodyParser = require('body-parser');

const app = express();

// eslint-disable-next-line no-unused-vars
const db = mongoose.connect('mongodb://localhost/bookapi');

const port = process.env.port || 3000;

const Book = require('./models/bookmodel');

const bookRouter = require('./routes/bookRouter')(Book);

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());

app.use('/api', bookRouter);
app.get('/', (req, res) => {
  res.send('Hello World...');
});

app.listen(port, () => {
  console.log(`Running On Port ${port}  `);
});
